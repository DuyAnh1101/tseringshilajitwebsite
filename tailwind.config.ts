import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      colors: {
        primary: '#c69409',
        primary_1: '#d2a12a',
        secondary: '#8a8a8a',
        secondary_1: '#f3f4f6',
      },
      borderColor: {
        primary: '#c69409',
      },
      backgroundColor: {
        primary: '#c69409',
      },
      fontFamily: {
        'authentic': ['Pacifico', 'cursive'],
        'Oswald': ['Oswald', 'sans-serif'],
      },
      backgroundImage: {
        'custom-gradient': 'linear-gradient(180deg, rgba(0, 0, 0, 1) 0%, rgba(255, 0, 0, 0) 43.07692307692308%)',
      },
      boxShadow: {
        button : '5px 5px 5px 0px #00000047'
      }
    },
  },
  plugins: [],
};
export default config;
