/** @type {import('next').NextConfig} */
const nextConfig = {
  async redirects() {
    return [
      {
        source: "/",
        destination: "/home",
        permanent: true, // This sets the redirect as a permanent (308) redirect. Use `false` for a temporary (307) redirect.
      },
    ];
  },
};

export default nextConfig;
