"use client";
import { Navbar } from "../components/navbar/Navbar";
import { Footer } from "../components/footer/Footer";
import StarIcon from "@mui/icons-material/Star";
import StarHalfIcon from "@mui/icons-material/StarHalf";
import BloodtypeIcon from "@mui/icons-material/Bloodtype";
import KeyboardDoubleArrowRightIcon from "@mui/icons-material/KeyboardDoubleArrowRight";
import "./Home.css";
import VideoSection from "../components/videoSection/VideoSection";
import BlogItem from "../components/blog-item/BlogItem";
import "aos/dist/aos.css";
import { useEffect, useState } from "react";
import CountUp from "react-countup";
import AOS from "aos";

const Home = () => {
  const [startCount, setStartCount] = useState(false);
  useEffect(() => {
    AOS.init();
    const handleScroll = () => {
      const element = document.getElementById("countup-section");
      if (element && element.getBoundingClientRect().top < window.innerHeight) {
        setStartCount(true);
        window.removeEventListener("scroll", handleScroll);
      }
    };

    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  }, []);
  const blogItems = [
    {
      id: 1,
      title: "The Power Of Fulvic Acid: Meet The Hidden Hero",
      content:
        "Welcome to a journey into the heart of nature’s own treasure – fulvic acid. Nestled within the ancient Shilajit, found [...]",
      img: "./images/article_4.jpg",
      href: "/",
    },
    {
      id: 2,
      title:
        "GMP Unleashed: The Rigorous Standards Behind Quality Of Our Premium Shilajit",
      content:
        "Unlock the secrets of GMP Certification: a mark of quality, safety, and excellence in our Shilajit products. Dive deep into [...]",
      img: "./images/article_3.jpg",
      href: "/",
    },
    {
      id: 3,
      title: "5 Key Myths Debunked: Your Guide To Buying Genuine Shilajit",
      content:
        "Unveiling Shilajit truths! Dive into a world where myths are debunked, quality is king, and science leads the way.",
      img: "./images/article_2.jpg",
      href: "/",
    },
    {
      id: 4,
      title:
        "Ensuring Purity: The Role Of Lab Testing In Verifying Shilajit Quality",
      content:
        "Discover why lab testing is crucial for Shilajit. Dive into purity, authenticity, and safety to ensure you're getting the best [...]",
      img: "./images/article_1.jpg",
      href: "/",
    },
  ];
  return (
    <>
      <Navbar />
      <div>
        <VideoSection src="./video/video_home.MP4" />
        <div className="section-background relative">
          <div className="flex flex-row w-full absolute top-1/4 px-20">
            <div className=" w-[53%] h-[70%]  flex gap-5 flex-col bg-[#00000085] rounded-[40px] px-[60px] pt-[30px] pb-[60px]">
              <p className="text-white font-bold tracking-[0.3rem]">
                MOUNTAINDROP® ORIGINAL
              </p>
              <h2 className="text-primary_1 text-[80px] font-thin leading-[70px] font-authentic-text">
                The Authentic Shilajit
              </h2>
              <p className="text-white width-[458px] leading-7">
                We are very serious about authenticity and this is why we
                provide only the purest shilajit in its original unprocessed
                natural form. No strange processes, no additives, just 100% pure
                shilajit resin
              </p>
            </div>
          </div>
        </div>
        <div className="flex flex-col py-20 px-60 bg-white">
          <div className="mx-auto">
            <a href="/reviews" className="text-2xl font-semibold">
              <span className="flex items-center text-[19px]">
                <StarIcon className="text-[#00b67a] text-5xl" />
                Trustpilot
              </span>{" "}
              <span>
                <StarIcon className="text-[#00b67a] text-5xl" />
                <StarIcon className="text-[#00b67a] text-5xl" />
                <StarIcon className="text-[#00b67a] text-5xl" />
                <StarIcon className="text-[#00b67a] text-5xl" />
                <StarHalfIcon className="text-[#00b67a] text-5xl" />
              </span>
              <br />
              <span className="text-xs ml-3 font-normal">
                TrustScore <span className="font-bold">4.7</span> |{" "}
                <span className="font-bold">461</span> reviews
              </span>
            </a>
          </div>
          <div className="flex gap-10 mx-auto pt-5">
            <div className="flex flex-col gap-6">
              <p className="text-primary_1 font-semibold">
                100% SHILAJIT RESIN
              </p>
              <h1 className="text-5xl font-semibold">What Is It?</h1>
              <p className=" text-[#5a5a5a]">
                Shilajit is a natural resin-like superfood that exudes from the
                unspoiled slopes of the Altai mountains. In the thousands of
                years of its creation, this ‘mountain pitch’ gets highly
                enriched with minerals, vitamins, and other vital properties.
              </p>
              <img
                className="min-w-[245px]"
                src="/images/altaihimalayasspoon-only-1.png"
                alt=""
              />
            </div>
            <img
              className="h-[490px]"
              src="/images/Trustpilot_image.png"
              alt=""
            />
            <div className="flex flex-col gap-4 px-5">
              <BloodtypeIcon className="text-4xl" />
              <h1 className="text-[15px] font-semibold">
                RICH IN VITAMINS & MINERALS
              </h1>
              <p className=" text-[#5a5a5a]">
                Shilajit in its original form contains a whole array of vitamins
                such as B1 – B12, A, C, E, and D3. Minerals like Iron,
                Potassium, Calcium, Zinc, Magnesium, Copper and many more. It
                also contains amino acids, fulvic acid, hippuric acid,
                phospholipids, DCPs & DBPs, 60 macro-and microelements. All of
                the above are what make Mountaindrop Shilajit such a powerful
                natural biostimulant.
              </p>
            </div>
          </div>
        </div>
        <div className="bg-black text-center pt-28 relative">
          <p className="text-primary_1 text-lg font-semibold mb-8 tracking-[2px] font-oswald">
            OUR SUPERIORITY
          </p>
          <h1 className="text-white text-[90px] font-thin leading-[70px] font-authentic-text">
            The Mountaindrop Difference
          </h1>
          <p className="text-white w-[700px] mx-auto mt-12 font-extralight mb-20 text-lg">
            Mountaindrop® sets the gold standard in authentic premium shilajit.
            We have conducted extensive research over the years and multiple
            tests to bring you the best quality shilajit in its purest resin
            form. We carefully source our shilajit from specific mountainous
            locations around the world reaching up to more than 5000 meters to
            harvest it.
          </p>
          <div className="text-center relative flex flex-col items-center">
            <p className="text-primary_1 font-semibold text-lg font-oswald tracking-[7px] mb-4">
              SHILAJIT HARVEST ALTITUDE
            </p>
            <img
              className="w-[500px] h-[500px]"
              src="/images/harvest.png"
              alt=""
            />
            <div className="text-white text-[110px] font-extralight absolute top-[40%] left-[36%]">
              <span>{">"}</span>
              <span id="countup-section">
                {startCount && (
                  <CountUp start={1} end={5000} duration={10} separator="" />
                )}
              </span>
              <span>m</span>
            </div>
          </div>
          <div className="relative flex justify-center">
            <img
              className="h-[590px] absolute top-[-50%] z-10"
              src="/images/md-mountain-v4-1.png"
              alt=""
              data-aos="fade-up"
              data-aos-duration="2000"
            />
            <div className="grid grid-cols-3 gap-8 z-20 pt-[275px] pb-40">
              <div className="text-white px-10 py-4 border border-[#d2a12a] rounded-lg">
                <h3 className="text-5xl font-extralight mb-2">85+</h3>
                <p className="text-sm font-light">Beneficial Nutrients</p>
              </div>
              <div className="text-white px-10 py-4 border border-[#d2a12a] rounded-lg">
                <h3 className="text-5xl font-extralight mb-2">18+</h3>
                <p className="text-sm font-light">Amino Acids</p>
              </div>
              <div className="text-white px-10 py-4 border border-[#d2a12a] rounded-lg">
                <h3 className="text-5xl font-extralight mb-2">Rich In</h3>
                <p className="text-sm font-light">Fulvic Acid</p>
              </div>
            </div>
          </div>
        </div>
        <div className="text-center relative items-center bg-black">
          <div className="grid grid-cols-2 px-[345px] gap-8 items-center">
            <div>
              <p className="text-primary font-semibold">
                THE MOST TRUSTED SHILAJIT
              </p>
              <h1 className="text-white text-5xl -tracking-[0.001px] font-Oswald mt-4">
                What Set Us Apart
              </h1>
            </div>
            <span className="text-secondary text-xl ">
              At Mountaindrop, we test and select only the most potent and
              nutrient-rich shilajit.
            </span>
          </div>
          <div
            className="grid grid-cols-4 px-[180px] gap-5 py-20"
            data-aos="fade-down"
          >
            <div className="flex flex-col items-start gap-2">
              <span className="text-primary text-2xl font-bold">01</span>
              <span className="text-white font-bold text-3xl">Quality</span>
              <p className="text-secondary text-left min-h-[320px]">
                Top quality, purity and authenticity are at the very core of our
                product range. Getting you the best quality of 100% pure
                Shilajit can be guaranteed on Mountaindrop since we gladly
                provide all the necessary certificates required to make such a
                statement. We test and select only the most potent and
                nutrient-rich shilajit.
              </p>
              <img
                src="/images/Quality_image.jpg"
                height={530}
                alt=""
                className="w-full aspect-[1/2] object-cover grayscale rounded-xl hover:grayscale-0"
              />
            </div>
            <div className="flex flex-col items-start gap-2" data-aos="fade-up">
              <span className="text-primary text-2xl font-bold">02</span>
              <span className="text-white font-bold text-3xl">Production</span>
              <p className="text-secondary text-left min-h-[320px]">
                We follow the highest standards in manufacturing to ensure
                excellence in every product. Our GMP-certified manufacturing
                process and our ISO-9001 certification are distinguished
                achievements that highlight our commitment to excellence. All
                our premium shilajit is hand-harvested and extracted using only
                the purest water and without applying any chemical compounds in
                the process.
              </p>
              <img
                src="/images/Production_image.png"
                height={530}
                alt=""
                className="w-full aspect-[1/2] object-cover grayscale rounded-xl hover:grayscale-0"
              />
            </div>
            <div
              className="flex flex-col items-start gap-2"
              data-aos="fade-down"
            >
              <span className="text-primary text-2xl font-bold">03</span>
              <span className="text-white font-bold text-3xl">Safety</span>
              <p className="text-secondary text-left min-h-[320px]">
                All Mountaindrop products are subjected to rigorous tests. Our
                Original shilajit is certified by the HACCP food safety standard
                since we test every single batch for: <br /> - Authenticity
                <br />- Purity
                <br /> - Heavy metals
                <br /> - Harmful bacteria
                <br /> Every jar of Mountaindrop Shilajit has a safety seal and
                LOT number to ensure product safety.
              </p>
              <img
                src="/images/Safety_image.jpg"
                height={530}
                alt=""
                className="w-full aspect-[1/2] object-cover grayscale rounded-xl hover:grayscale-0"
              />
            </div>
            <div className="flex flex-col items-start gap-2" data-aos="fade-up">
              <span className="text-primary text-2xl font-bold">04</span>
              <span className="text-white font-bold text-3xl">Storage</span>
              <p className="text-secondary text-left min-h-[320px]">
                We store our shilajit only in special violet glass containers
                with biophotonic properties that protect the raw material and
                provide a much longer life span while preserving its beneficial
                properties. In this way, shilajit can be stored for longer while
                still being completely healthy and safe for consumption.
              </p>
              <img
                src="/images/Storage_image.png"
                height={530}
                alt=""
                className="w-full aspect-[1/2] object-cover grayscale rounded-xl hover:grayscale-0"
              />
            </div>
          </div>
        </div>
        <div className="bg-black">
          <div className="text-center bg-[url('/images/aesthetic-black-ROCKS-1-1.jpg')] py-20 bg-cover relative">
            <div className="gradient-overlay"></div>
            <div className="content">
              <h1 className="font-authentic-text text-7xl pb-10 text-white">
                Experience The Authentic Shilajit
              </h1>
              <p className="text-secondary_1 font-extralight w-[400px] text-xl mx-auto">
                We select and source only the best shilajit of the highest
                quality from around the world
              </p>
            </div>
          </div>
          <div className="relative ">
            <img
              src="/images/productIntroView1.jpg"
              alt=""
              className="w-[770px] mx-auto"
            />
            <div className="gradient-overlay "></div>
            <div className="absolute top-14 left-[28%] content z-10 h-[85%] flex justify-between flex-col items-center">
              <div className="flex w-[700px] justify-between items-start">
                <div>
                  <span className="text-white text-2xl font-light">
                    Original <br />
                    <span className="font-semibold">HIMALAYAS</span>
                  </span>
                  <div className="flex">
                    <span className="text-white font-medium mt-7">
                      Available in:
                    </span>
                    <img
                      className="invert-[.67]"
                      src="/images/ORIGINAL-40gB.png"
                      width={85}
                      alt=""
                    />
                  </div>
                </div>
                <div className="flex bg-[#00000036] px-4 pt-4 rounded-3xl">
                  <img
                    src="/images/MOUNTAIN-gold-HIMALAYAS-final-1.png"
                    alt=""
                    width={100}
                  />
                  <div>
                    <h1 className="text-primary font-semibold text-sm">
                      HARVEST ALTITUDE
                    </h1>
                    <p className="text-white text-lg font-semibold">{`>5000m`}</p>
                  </div>
                </div>
              </div>
              <a className="bg-black text-white border-2 border-white p-3 font-medium rounded-[30px] shadow-button w-1/5 flex items-center gap-2 cursor-pointer hover:bg-primary hover:text-white hover:scale-110 transition duration-300 ease-in-out">
                <KeyboardDoubleArrowRightIcon />
                Order now
              </a>
            </div>
          </div>
        </div>
        <div className="bg-[url('/images/shilajit-5.jpg')] h-[550px] bg-[length:900px_auto] bg-no-repeat bg-right contrast-125 relative bg-white">
          <div className="absolute bottom-20 left-[20%] flex flex-col gap-10">
            <img
              src="/images/shutterstock_2193102391-1.jpg"
              className="scale-110 border-2 border-[#4A4A4A57] rounded-3xl"
              alt=""
              width={120}
            />
            <div className="text-primary font-bold text-lg tracking-[0.2em]">
              SECRETS UNLOCKED
            </div>
            <p className="text-4xl font-extrabold tracking-widest leading-normal">
              FROM COLOR TO <br /> CONSISTENCY: <br /> HOW TO IDENTIFY <br />{" "}
              PURE SHILAJIT
            </p>
          </div>
        </div>
        <div className="bg-black flex   px-10 py-20">
          <div className="mx-auto flex gap-10">
            {blogItems.map(({ id, title, content, img, href }) => (
              <BlogItem
                href={href}
                key={id}
                id={id}
                title={title}
                content={content}
                img={img}
              />
            ))}
          </div>
        </div>
        <div className="bg-[url('/images/section_mountain.png')] h-[540px] bg-no-repeat bg-cover bg-[center_top_-160px] relative flex">
          <div className="flex m-auto gap-20">
            <div className="bg-[#000000B3] flex flex-col px-16 pb-16 pt-8 gap-10 rounded-[40px]">
              <p className="text-white font-bold tracking-[0.3rem]">
                MOUNTAINDROP® ORIGINAL
              </p>
              <h2 className="text-primary_1 text-[80px] font-thin leading-[70px] font-authentic-text">
                Beneficial Effects
              </h2>
              <p className="text-white w-[458px] leading-7 text-lg">
                Shilajit is a unique natural superfood thanks to its rich
                content in minerals, vitamins and other beneficial nutrients.
                Learn how shilajit can boost your performance enhance your day!
              </p>
              <a
                href="/beneficial-effects"
                className="bg-transparent text-white border-2 w-[26%] border-white px-4 py-2 text-base font-medium rounded-[30px] shadow-button cursor-pointer hover:text-primary hover:border-primary hover:scale-110 transition duration-300 ease-in-out text-center"
              >
                Read more
              </a>
            </div>
            <div className="bg-[#000000B3] flex flex-col px-16 pb-16 pt-8 gap-10 rounded-[40px]">
              <p className="text-white font-bold tracking-[0.3rem]">
                MOUNTAINDROP® ORIGINAL
              </p>
              <h2 className="text-primary_1 text-[80px] font-thin leading-[70px] font-authentic-text">
                How To Use
              </h2>
              <p className="text-white w-[458px] leading-7 text-lg">
                Mountaindrop® shilajit resin can be used not only as a powerful
                food supplement but also as an effective cosmetic product!
                Discover how you can get the most out of your shilajit.
              </p>
              <a
                href="/how-to-use"
                className="bg-transparent text-white border-2 w-[26%] border-white px-4 py-2 text-base font-medium rounded-[30px] shadow-button cursor-pointer hover:text-primary hover:border-primary hover:scale-110 transition duration-300 ease-in-out text-center"
              >
                Read more
              </a>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};

export default Home;
