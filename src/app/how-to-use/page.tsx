import React from "react";
import { Navbar } from "../components/navbar/Navbar";
import { Footer } from "../components/footer/Footer";
import "./Guide.css";
import VideoSection from "../components/videoSection/VideoSection";

const Guide = () => {
  return (
    <div>
      <Navbar />
      <h1 className="mt-28 text-center text-4xl text-pr">This is a video tutorial</h1>

      <div className="w-1/3 mb-40 flex mx-auto">
        <VideoSection src="./video/how-to-use.MP4" />
      </div>
      <Footer />
    </div>
  );
};

export default Guide;
