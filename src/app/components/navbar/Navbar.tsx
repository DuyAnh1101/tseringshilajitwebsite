import React from "react";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import "./Navbar.css";
export const Navbar = () => {
  return (
    <div className="top-0 fixed flex bg-black w-full z-30">
      <div className="flex items-center mx-auto gap-8">
        <ul className="flex gap-6 items-center">
          <li className="relative group">
            <div className="text-white hover:text-primary p-2 font-light cursor-pointer">
              Our shilaijit
              <KeyboardArrowDownIcon />
            </div>
            <div className="bg-white px-4 py-5 absolute w-[150%] rounded-lg shadow-md hidden group-hover:block">
              <ul>
                <li className="cursor-pointer text-black hover:text-primary px-3 py-2">
                  <a href="/our-shilajit">100% Authentic Shilajit</a>
                </li>
                <li className="cursor-pointer text-black hover:text-primary px-3 py-2">
                  <a href="/how-to-use">How to use</a>
                </li>
              </ul>
            </div>
          </li>

          <li className="relative group">
            <div className="text-white hover:text-primary p-2 font-light cursor-pointer">
              Know us
              <KeyboardArrowDownIcon />
            </div>
            <div className="bg-white px-4 py-5 absolute w-[170%] rounded-lg shadow-md hidden group-hover:block">
              <ul>
                <li className="cursor-pointer text-black hover:text-primary px-3 py-2">
                  <a href="/our-story">Our story</a>
                </li>
                <li className="cursor-pointer text-black hover:text-primary px-3 py-2">
                  <a href="/blog">Blog</a>
                </li>
                <li className="cursor-pointer text-black hover:text-primary px-3 py-2">
                  <a href="/contact">Contact</a>
                </li>
              </ul>
            </div>
          </li>
          <li>
            <a
              href="/blog"
              className="text-white hover:text-primary p-2 font-light"
            >
              Blog
            </a>
          </li>
        </ul>
        <a href="/home">
          <img
            src="./images/logo.jpg"
            alt=""
            className="h-16 my-2 mx-10 rounded-full border border-[#01120A]"
          />
        </a>
        <div className="relative group">
          <span className="text-white flex gap-3 items-center mx-14 text-sm font-semibold cursor-pointer">
            <img
              src="./images/Flag_of_the_United_Kingdom.svg.webp"
              alt="English"
              className="w-[30px]"
            />
            EN
            <KeyboardArrowDownIcon />
          </span>
          <div className="bg-white px-2 py-1 absolute w-[70%] rounded-lg shadow-md hidden group-hover:block left-12">
            <ul>
              <li className="cursor-pointer text-black hover:text-primary px-0.5 py-2 flex items-center gap-3">
                <img
                  src="./images/Flag_of_the_United_Kingdom.svg.webp"
                  alt="English"
                  className="w-[20px]"
                />
                <a href="/?lang=en">English</a>
              </li>
              <li className="cursor-pointer text-black hover:text-primary px-0.5  py-2 flex items-center gap-3">
                <img
                  src="./images/Flag_of_Vietnam.svg.png"
                  alt="Vietnamese"
                  className="w-[20px]"
                />
                <a href="/?lang=vi">Vietnamese</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};
