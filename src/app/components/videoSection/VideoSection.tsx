import React from "react";
import "./VideoSection.css";

interface VideoSectionProps {
  src: string;
  height?: number;
}

const VideoSection: React.FC<VideoSectionProps> = ({ src, height = 1519 }) => {
  return (
    <div className="video-container">
      <video
        height={height}
        controls
        autoPlay
        loop
        muted
        className="video mb-20"
      >
        <source src={src} type="video/mp4" />
        Your browser does not support the video tag.
      </video>
    </div>
  );
};

export default VideoSection;
