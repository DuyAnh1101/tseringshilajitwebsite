import React from "react";

interface BlogItemProps {
  id: number;
  title: string;
  content: string;
  img: string;
  href: string
}
const BlogItem: React.FC<BlogItemProps> = ({ id, title, content, img, href }) => {
  return (
    <div key={id} className="flex flex-col justify-around  w-[190px] gap-10">
      <div className="flex flex-col gap-10">
        <a href={href}>
          <img
            src={img}
            alt=""
            className="w-[230px] h-[106px] object-cover rounded-2xl"
          />
        </a>
        <a href={href}>
          <h1 className="text-white">{title}</h1>
        </a>
        <p className="text-secondary text-sm">{content}</p>
      </div>
      <a
        href={href}
        className="border-primary px-4 py-2 hover:bg-primary text-primary hover:text-white border text-center cursor-pointer rounded-full mt-auto"
      >
        Read more
      </a>
    </div>
  );
};

export default BlogItem;
