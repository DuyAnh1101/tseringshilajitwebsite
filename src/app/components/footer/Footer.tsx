import React from "react";
import FacebookIcon from "@mui/icons-material/Facebook";
import TwitterIcon from "@mui/icons-material/Twitter";
import InstagramIcon from "@mui/icons-material/Instagram";
import PinterestIcon from "@mui/icons-material/Pinterest";

export const Footer = () => {
  return (
    <div className=" bg-black text-white flex flex-col pt-20 pb-10">
      <div className="mx-auto flex flex-row gap-20 px-10 py-12">
        <div className="flex flex-col gap-10">
          <img
            src="./images/MOUNTAINDROP-LOGO-NO-ICON-L.png"
            alt=""
            className="w-[300px]"
          />
          <p className="w-[377px] text-secondary">
            We source and deliver authentic shilajit of the highest quality and
            produce natural wellness products based on this marvelous superfood
          </p>
          <div>
            Discover more
            <div className="text-secondary mt-5">
              <a href="/" className="p-2.5">
                <FacebookIcon className="transform transition-transform duration-200 hover:scale-125 text-3xl" />
              </a>
              <a href="/" className="p-2.5">
                <InstagramIcon className="transform transition-transform duration-200 hover:scale-125 text-3xl" />
              </a>
              <a href="/" className="p-2.5">
                <PinterestIcon className="transform transition-transform duration-200 hover:scale-125 text-3xl" />
              </a>
              <a href="/" className="p-2.5 ">
                <TwitterIcon className="transform transition-transform duration-200 hover:scale-125 text-3xl" />
              </a>
            </div>
          </div>
        </div>
        <div className="flex flex-col gap-10 border-l border-[#797e8a] pl-[30px]">
          <h1 className="text-[#cecece] text-2xl font-extrabold">About</h1>
          <ul className="text-secondary ">
            <li>
              <a className="hover:text-white" href="/">
                Our story
              </a>
            </li>
            <li>
              <a className="hover:text-white" href="/">
                Our shilajit
              </a>
            </li>
            <li>
              <a className="hover:text-white" href="/">
                How to use
              </a>
            </li>
            <li>
              <a className="hover:text-white" href="/">
                Contact us
              </a>
            </li>
          </ul>
          <a
            href="/"
            className="text-xl font-extrabold text-[#797e8a] hover:text-white"
          >
            See our <br /> distributors
          </a>
        </div>
        <div className="flex flex-col gap-10 pl-[30px]">
          <h1 className="text-[#cecece] text-2xl font-extrabold">Store</h1>
          <ul className="text-secondary">
            <li>
              <a className="hover:text-white" href="/">
                Shop
              </a>
            </li>
            <li>
              <a className="hover:text-white" href="/">
                Login
              </a>
            </li>
            <li>
              <a className="hover:text-white" href="/">
                Register
              </a>
            </li>
            <li>
              <a className="hover:text-white" href="/">
                FAQ
              </a>
            </li>
          </ul>
          <ul className="text-secondary">
            <li>
              <a className="hover:text-white mb-2" href="/">
                privacy policy
              </a>
            </li>
            <li>
              <a className="hover:text-white mb-2" href="/">
                terms & conditions
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div className="flex mx-auto">
        <img className="w-28" src="./images/MD_Badge_AAA_2023_BG.png" alt="" />
        <img className="w-28" src="./images/MD_Badge_DUNS_BG.png" alt="" />
        <img className="w-28" src="./images/MD_Badge_GMP_BG.png" alt="" />
        <img className="w-28" src="./images/MD_Badge_HACCP_BG.png" alt="" />
        <img className="w-28" src="./images/MD_Badge_Iqnet_BG.png" alt="" />
        <img className="w-28" src="./images/MD_Badge_ISO9001_BG.png" alt="" />
        <img className="w-28" src="./images/MD_Badge_SIQ_BG.png" alt="" />
        <img
          className="w-28"
          src="./images/MD_Badge_Trustpilot_BG.png"
          alt=""
        />
      </div>
    </div>
  );
};
